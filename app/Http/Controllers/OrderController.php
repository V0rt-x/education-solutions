<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Menu;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $menusToSave = $request->get('menus');

        $dbMenus = Menu::find(array_keys($menusToSave));

        foreach ($dbMenus as $menu) {
            $newOrder = Order::create([
                'mealtime_id' => $menu->mealtime->id,
                'user_id' => 0
            ]);

            $newOrder->dishes()->attach($menusToSave[$menu->id]);
        }
    }
}
