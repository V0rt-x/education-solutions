<?php


namespace App\Http\Controllers;


use App\Dish;
use App\DishType;
use App\MealType;
use Illuminate\Support\Facades\Redirect;

class DishController
{
    public function index()
    {
        $dishes = Dish::all();

        return view('dishes.index', compact('dishes'));
    }

    public function create()
    {
        $mealTypes = MealType::all()->pluck('code', 'id');
        $dishTypes = DishType::all()->groupBy('meal_type_id')->keyBy(function ($value, $key) use ($mealTypes) {
            return $mealTypes[$key];
        });

//        $mealTypes->get('code')

        dd($mealTypes);

//        dd($dishTypes);

        return view('dishes.create', compact('dishTypes', 'mealTypes'));
    }

    public function store()
    {
        return Redirect::action('/dishes');
    }
}
