<?php

namespace App\Http\Controllers;

use App\MealType;
use App\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::getMealTypesByMenus(Menu::thisWeeks()->get());

        view('menus.index', compact('menus'));
    }

    public function show(Menu $menu)
    {
        //
    }

    public function store()
    {
        //
    }
}
