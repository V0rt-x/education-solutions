<?php

namespace App\Http\Controllers;

use App\Date;
use Carbon\Carbon;

class DateController extends Controller
{
    const DB_DATE_FORMAT = 'Y-m-d';

    public function index()
    {
        $dates = Date::all();

        return view('dates.index', compact('dates'));
    }

    public function show($id)
    {
        $date = Date::findOrFail($id);

        return view('dates.show', [
            'date' => $date,
            'previousDate' => Date::where('date', '=', $date->carbonDate()->subDay()->format(self::DB_DATE_FORMAT))->first(),
            'followingDate' => Date::where('date', '=', $date->carbonDate()->addDay()->format(self::DB_DATE_FORMAT))->first()
        ]);
    }
}
