<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DishCategory
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DishCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DishCategory extends Model
{
    //
}
