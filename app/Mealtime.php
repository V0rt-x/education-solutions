<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mealtime
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mealtime whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mealtime extends Model
{
    //
}
