<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MenuSectionType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSectionType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MenuSectionType extends Model
{
    //
}
