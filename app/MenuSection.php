<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MenuSection
 *
 * @property int $id
 * @property int $menu_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $menu_section_type_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dish[] $dishes
 * @property-read int|null $dishes_count
 * @property-read \App\MenuSectionType $menuSectionType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuSection whereMenuSectionTypeId($value)
 */
class MenuSection extends Model
{
    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }

    public function menuSectionType()
    {
        return $this->belongsTo(MenuSectionType::class);
    }
}
