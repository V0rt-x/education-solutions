<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Menu
 *
 * @property int $id
 * @property string $date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|\App\Menu newModelQuery()
 * @method static Builder|\App\Menu newQuery()
 * @method static Builder|\App\Menu query()
 * @method static Builder|\App\Menu whereCreatedAt($value)
 * @method static Builder|\App\Menu whereDate($value)
 * @method static Builder|\App\Menu whereId($value)
 * @method static Builder|\App\Menu whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int $week_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MealType[] $mealTypes
 * @property-read int|null $meal_types_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu thisWeeks()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereWeekId($value)
 * @property int $date_id
 * @property int $mealtime_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dish[] $dishes
 * @property-read int|null $dishes_count
 * @property-read \App\Mealtime $mealtime
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereDateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereMealtimeId($value)
 */
class Menu extends Model
{
    public function mealtime()
    {
        return $this->belongsTo(Mealtime::class);
    }

    public function dishes()
    {
        return $this->belongsToMany(Dish::class);
    }

    /**
     *  @param Builder $query
     *  @return Builder
     */
    public function scopeThisWeeks($query)
    {
        return $query->whereBetween('date', [Carbon::now()->startOfWeek(Carbon::MONDAY), Carbon::now()->endOfWeek(Carbon::SUNDAY)]);
    }

    public function setDaysNames($menus)
    {
        foreach ($menus as &$menu) {
            $menu['dayName'] = Carbon::make($menu->date)->locale('ru_RU')->dayName;
        }

        return $menus;
    }
}
