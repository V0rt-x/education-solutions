<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Date
 *
 * @property int $id
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Date whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Menu[] $menus
 * @property-read int|null $menus_count
 */
class Date extends Model
{
    const DATE_PRINT_FORMAT = 'j F';
    const DB_DATE_FORMAT = 'Y-m-d';

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function carbonDate($format = self::DB_DATE_FORMAT): Carbon
    {
        return Carbon::createFromFormat($format, $this->date);
    }

    public function dateToPrint()
    {
        return Carbon::createFromFormat(self::DB_DATE_FORMAT, $this->date)->format(self::DATE_PRINT_FORMAT);
    }
}
