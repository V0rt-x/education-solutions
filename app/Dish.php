<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Dish
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $dish_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereDishTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $dish_category_id
 * @property-read \App\DishCategory|null $dishCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dish whereDishCategoryId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 */
class Dish extends Model
{
    public function dishCategory()
    {
        return $this->belongsTo(DishCategory::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
}
