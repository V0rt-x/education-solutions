<?php

return [
    'meal_types' => [
        'map' => [
            'B' => 'breakfast',
            'D' => 'dinner',
            'S' => 'snack'
        ],
        'default' => [
            'B', 'D', 'S'
        ],
        'available' => [
            'D'
        ]
    ],
    'dish_types' => [
        'map' => [
            'M' => 'main',
            'G' => 'garnish',
            'S' => 'soup',
            'A' => 'salad',
            'D' => 'drink',
            'I' => 'side',
            'F' => 'fruit'
        ],
        'default' => [
            'M', 'G', 'S', 'A', 'D', 'I'
        ],
        'available' => [
            'M', 'G', 'S', 'A'
        ]
    ]
];
