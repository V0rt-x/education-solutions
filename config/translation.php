<?php

return [
    'to_russian' => [
        'breakfast' => 'завтрак',
        'dinner' => 'обед',
        'snack' => 'полдник'
    ],
    'to_english' => [
        'завтрак' => 'breakfast',
        'обед' => 'dinner',
        'полдник' => 'snack'
    ]
];
