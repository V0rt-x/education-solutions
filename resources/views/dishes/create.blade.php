@extends('layout')

@section('content')

    <form action="/dishes">
        <div class="meal-types-selector">
            @foreach($mealTypes as $mealType)
                <label>
                    <input name="meal_type" type="radio" value="{{ $mealType->code }}">{{ $mealType->name }}
                </label>
            @endforeach
        </div>

        <div class="dish-types-selector">
            <select>
                @foreach($dishTypes as $dishType)
                    <option value="{{ $dishType->code }}">
                        {{ $dishType->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <input type="submit">
    </form>

@endsection
