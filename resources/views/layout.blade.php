<!doctype html>
<html lang="ru">
<head>
    <title>TEST</title>
    <link rel="stylesheet" href="/css/table.css"/>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>
</head>
<body>

@yield('content')

</body>
</html>

