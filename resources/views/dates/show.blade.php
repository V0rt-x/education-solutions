@extends('layout')

@section('content')
    <ul>
        <li><a href="{{ $previousDate->id }}">{{ $previousDate->dateToPrint() }}</a></li>
        <li><a href="{{ $date->id }}">{{ $date->dateToPrint() }}</a></li>
        <li><a href="{{ $followingDate->id }}">{{ $followingDate->dateToPrint() }}</a></li>
    </ul>
    <form action="/orders/" method="post">
        @csrf
        <table border="1">
            @foreach($date->menus as $menu)
                <tr>
                    <td>{{ $menu->mealtime->name }}</td>
                    <td>
                        @foreach($menu->dishes->groupBy('dish_category_id') as $dishCategory)
                            <a>{{ $dishCategory->first()->dishCategory->name }}</a>
                            <label>
                                <select name="menus[{{ $menu->id }}][]">
                                    @foreach($dishCategory as $dish)
                                        <option value="{{ $dish->id }}">{{ $dish->name }}</option>
                                    @endforeach
                                </select>
                            </label>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </table>
        <input type="submit" value="Сохранить">
    </form>
@endsection

