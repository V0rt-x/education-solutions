@extends('layout')

@section('content')
    <div class="container">
        <h2>Меню на текущий месяц</h2>
        <ul class="responsive-table">
            @foreach ($weeks as $week)
                <li class="table-row">
                    <a href="weeks/{{ $week->id }}">
                        <div class="col col-1"></div>
                        <div class="col col-2"></div>
                        <div class="col col-3">{{ $week->date_from }}</div>
                        <div class="col col-4">{{ $week->date_to }}</div>
                    </a>
                </li>
            @endforeach
            <li class="table-row new-week">
                <a href="weeks/create">
                    <div class="col col-1">Добавить</div>
                </a>
            </li>
        </ul>
    </div>
@endsection

