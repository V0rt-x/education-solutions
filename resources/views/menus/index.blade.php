@extends('layout')

@section('table')
    <table class="responsive_table">
        <tr>
            <th></th>
            @foreach($menus as $menu)
                <th>{{}}</th>
            @endforeach
        </tr>

        @foreach($mealTypes as $mealType)
            <tr>
                <td>{{$mealType->name}}</td>
                @foreach($menus as $menu)
                    <td>{{$menu->mealTypes->dishType->}}</td>
                @endforeach
            </tr>
        @endforeach
    </table>
@endsection
