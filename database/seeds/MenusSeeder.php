<?php

use App\Menu;
use App\Week;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class MenusSeeder extends Seeder
{
    const NUMBER_OF_DAYS = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weekIterator = Carbon::now();

        factory(Week::class, 4)
            ->make()
            ->each(function (Week $week) use (&$weekIterator) {
                $week->fill([
                    'date_from' => (string)$weekIterator->startOfWeek(),
                    'date_to' => (string)$weekIterator->endOfWeek()
                ])->save();

                $weekdaysIterator = $weekIterator->startOfWeek();

                factory(Menu::class, self::NUMBER_OF_DAYS)
                    ->make()
                    ->each(function (Menu $menu) use ($week, &$weekdaysIterator) {
                        $menu->fill([
                            'date' => $weekdaysIterator,
                            'week_id' => $week->id
                        ])->save();

                        $weekdaysIterator->addDay();
                    });

                $weekIterator->addWeek();
            });
    }
}
