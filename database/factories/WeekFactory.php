<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Week;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(Week::class, function (Faker $faker) {
    $now = Carbon::now();

    return [
        'date_from' => $now->startOfWeek(),
        'date_to' => $now->endOfWeek()
    ];
});
