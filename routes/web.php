<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('menus', 'MenuController@index');
Route::get('menus/{menu}', 'MenuController@show');

Route::get('dates', 'DateController@index');
//Route::get('dates/create', 'WeekController@create');
Route::get('dates/{id}', 'DateController@show');

Route::get('dishes', 'DishController@index');
Route::get('dishes/create', 'DishController@create');

Route::resource('orders', 'OrderController', ['only' => ['store']]);
